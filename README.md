# CS6500_Final_Project

## This is a project - Employee Salary Prediction and Analysis.

### The Repository contains the zip file which has following files and Folders
    dataset.zip - contains the raw data (dataset.csv and salaries.csv files.)
    Employees Salary Prediction and Analysis.pdf
    Project.ipynb
    README.md


## Data Cleaning
    The first step is cleaning and converting the data into usable format using Spark SQL Queries.
    We have used the Docker image ("jupyter/all-spark-notebook") for this project which was provided by you to do the spark assignments.
    dataset.zip Zip file of Dataset, contains dataset.csv and salaries.csv files.

## Training and Testing the Model using Jupyter Lab
    We have used the Docker image ("jupyter/all-spark-notebook") for this project which was provided by you to do the Spark and SparkMl assignments.
    Open the Jupyter notebook after starting the namenode.
    We read the files from the folder "project" to the Project.ipynb file

### After reading the data, perform the line by line execution to test the code.

Note: perform the "pip install findspark" (and necessary packages if needed ) in the terminal of Jupyter Lab to avoid the module errors (restart kernel after installing packages)